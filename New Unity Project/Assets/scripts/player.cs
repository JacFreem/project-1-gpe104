﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class player : MonoBehaviour
{
    private Transform tf; // A variable to hold our Transform component
    Vector3 myVector = new Vector3(2, 4, 12);
   
    void Start()
    {
        // Get the Transform Component
        tf = GetComponent<Transform>();
        tf.position = Vector3.zero; // Sends object to an origin point
    }

    void Update()
    {
        // Move up every frame 
        tf.position = tf.position + Vector3.up; // moves the sprite on the y axis one time per frame
    }
}
